#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt 
ifoColors = ['darkgoldenrod','darkkhaki','darkseagreen','olive','cadetblue','green','slategray','darkcyan']


def plot_repeats(runFlag, figpath = None, chain = 0):
    # Takes in chain file name, computes wavelets repeated and makes a plot
    # Plots histogram of that information
    # should only be run when the glitch model is on (and no chirplets)

    fig, ax = plt.subplots()
    for i, ifo in enumerate(runFlag.ifoNames):
        print("chain name = ", runFlag.get_chain_name())
        try:
            wf = get_wavelet_params(f"{runFlag.trigdir}/chains/glitch_params_{ifo}.dat.{chain}", model = 'glitch', 
            burnin = 'half') # I think it's ok to hardcode glitch into the filename because this is only done for the glitch model
        except:
            try:
                wf = get_wavelet_params(f"{runFlag.trigdir}/chains/full_params_{ifo}.dat.{chain}", model = 'glitch', 
                burnin = 'half') # for full model
            except:
                try:
                    wf = get_wavelet_params(f"{runFlag.trigdir}/chains/cbc_params_{ifo}.dat.{chain}", model = 'glitch', 
                    burnin = 'half') # for CBC glitch runs
                except:
                    print("Couldn't get glitch files for repeat plot.\n")
                    return(1)
        repeats, percent_unique = get_repeats(wf)
        # Get weighted average
        sum_z2 = 0
        for r in repeats:
            sum_z2 += r ** 2
        weight_avg = sum_z2 / sum(repeats)

        if max(repeats) < 1:
            bins = 1
        else:
            bins = np.arange(-0.5, max(repeats) + 0.5, 1)
        ax.hist(repeats, bins = bins, density = False, label = "%s %.4g percent unique wavelets" % (ifo,percent_unique), alpha = 0.5, color = ifoColors[i])
    ax.set_xlabel("Number of repeats")
    ax.set_ylabel("Number of wavelets")
    ax.set_title("Rate of glitch wavelet repitition")
    ax.legend()

    if figpath is not None:
        plt.savefig(figpath)
    return


def get_repeats(glitch_dict):
    """
    Takes in output of the get_wavelet_params function 
    Outputs list containing the number of repeated values for each glitch waveform
    # TODO, will not work for chirplets 
    """
    sum_D = sum(glitch_dict['D'])
    wavelet_params = fill_wavelet_matrix(glitch_dict)

    percent_unique = 100 * get_unique_wavelet_list(wavelet_params) / sum_D
    print(f"Percent unique {percent_unique}%")

    # Holds onto info about whether that value has been repeated 
    been_repeated = np.zeros(sum_D)        
    repeats = []
    for i in range(sum_D):
        if i%10000 == 0:
            print(f"Repeats is {len(repeats)} long and is {100 * sum(repeats) / sum_D} % done")
            print(f"{i} of {sum_D}")
        if been_repeated[i] != 0:
            continue
        w = np.where(np.all(np.isclose(wavelet_params.T - wavelet_params[:, i], 0), axis=1))
        
        repeats.append(len(w[0]))
        been_repeated[w] = np.ones(len(w[0]))
        
    return repeats, percent_unique

def fill_wavelet_matrix(glitch_dict):
    sum_D = sum(glitch_dict['D'])
    wavelet_params = np.zeros((5, sum_D))
    i = 0
    for key in glitch_dict.keys():
        if key == 'D':
            continue
        wavelet_params[i] = glitch_dict[key]
        i += 1
    return wavelet_params


def get_unique_wavelet_list(wavelet_params):
    unique_rows = np.unique(wavelet_params, axis=1)
    print(f"get_unique_wavelet_list {np.shape(unique_rows)}")
    
    return np.shape(unique_rows)[1] 

def get_wavelet_params(filename, model, chirpflag=False, O1version=False, **keyword_parameters):
    """
    Read in chain file and get all wavelet params

    arguments
    ---------
    filename (str): the chain file

    model (str): signal or glitch

    optional, chirpflag: True if using chirplets
    optional, O1version: True if using O1 era chains
    optional, restrict (int): line number if you only want one draw from the chain
    optional, burnin: skip first N set of wavelets (or burnin = 'half' slips the first half)

    outputs
    -------
    dictionary of the wavelet params
    """
    NW = 5 # number of intrinsic parameters (changes for chirplets)
    NE = 6 # number of extrinsic parameters
    start = 1

    labels = ['t','f','Q','logA','phi_int'] # parameters of individual wavelets
    extlabels = ['alpha','sindelta','psi','elip', 'phi_ext','scale'] # Common extrinsic parameters
    if chirpflag:
        NW = 6
        labels.append('beta')

    data = {}
    for l in labels:
        data[l] = []

    if model == 'signal': # get extrinsic parameters
        for l in extlabels:
            data[l] = []

    data['D'] = []

    infile = open(filename)
    lines = infile.readlines()

    if ('restrict' in keyword_parameters):
        restrict = int(keyword_parameters['restrict'])
        rn = [restrict]
    elif ('burnin' in keyword_parameters):
        if keyword_parameters['burnin'] == 'half':
            burnin = int(len(lines) // 2)
        else:
            burnin = keyword_parameters['burnin']
        rn = np.arange(burnin, len(lines))
    else:
        rn = np.arange(0,len(lines))


    for j in rn:
        line = lines[j]
        spl = line.split()
        waveletnumber = int(spl[0]) # how many wavelets
        data['D'].append(waveletnumber)
        if model == 'signal':
            if waveletnumber > 0: # only do this if there are signal wavelets active
                start = NE+1 # extra parameters
                if O1version:
                    start += 1
                for l in range(0,NE):
                    data[extlabels[l]].append(float(spl[l+1]))
        for i in range(0,waveletnumber):
            for l in range(0,NW):
                if labels[l] == 'logA':
                    data[labels[l]].append(np.log10(float(spl[start+i*NW+l])))
                else:
                    data[labels[l]].append(float(spl[start+i*NW+l]))

    return data

